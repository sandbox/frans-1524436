<?php

function hookio_settings($form, &$form_state) {

   $form['hookio_id'] = array(
    '#title' => t('Hookio Identifier'),
    '#description' => t('The identifier for this hookio server.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('hookio_id', variable_get('site_name')),
  );
  $form['hookio_machinename'] = array(
    '#type' => 'machine_name',
    '#default_value' => variable_get('hookio_machinename', ''),
    '#machine_name' => array(
      'exists' => 'hookio_machinename_exists',
      'source' => array('hookio_id'),
    ),
  );

  $form['hookio_host'] = array(
    '#title' => t('Hookio Host'),
    '#description' => t('The host is a ipaddress or hostname.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('hookio_host', 'localhost'),
  );
  $form['hookio_port'] = array(
    '#title' => t('Hookio Port'),
    '#description' => t('The port for the hookio communication.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('hookio_port', 5000),
  );
  $form['hookio_mdns'] = array(
    '#title' => t('Hookio mDNS'),
    '#description' => t('Use mDNS.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('hookio_mdns', 0),
  );
  $form['hookio_authorize'] = array(
    '#title' => t('Hookio Authorize'),
    '#description' => t('Use authorization.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('hookio_authorize', '0'),
  );
  $form['hookio_user'] = array(
    '#title' => t('Hookio User'),
    '#description' => t('The user for authorization.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('hookio_user'),
    '#states' => array(
      'visible' => array(
        ':input[name="hookio_authorize"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['hookio_password'] = array(
    '#title' => t('Hookio Password'),
    '#description' => t('The password for authorization (leave empty for no changes).'),
    '#type' => 'textfield',
    '#default_value' => '',
    '#states' => array(
      'visible' => array(
        ':input[name="hookio_authorize"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['hookio_server_role'] = array(
    '#title' => t('Hookio Server Role'),
    '#description' => t('The role for this hookio server.'),
    '#type' => 'radios',
    '#options' => array(
      'start' => t('Auto'),
      'listen' => t('Master'),
      'connect' => t('Connect to a master'),
    ),
    '#default_value' => variable_get('hookio_server_role', 'start'),
  );
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advanced']['hookio_master_config'] = array(
    '#title' => t('Hookio master config file'),
    '#description' => t('JSON file that stores all instances for Hookio.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('hookio_master_config', '/tmp/drupal-hookio-forever.json'),
  );
  $form['advanced']['hookio_watch_file_dir'] = array(
    '#title' => t('Hookio Watch file directory'),
    '#description' => t('Directory for storing a watch file for Hookio.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('hookio_watch_file_dir', '/tmp'),
  );
  $form['advanced']['hookio_drush_path'] = array(
    '#title' => t('Drush executable'),
    '#description' => t('Location of the drush executable.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('hookio_drush_path', '/usr/bin/drush'),
  );

  $markup = '<h3 class="hookio-running">Status: ';
  if ($since = hookio_is_running()) {
    $markup .= t('Hookio is running since: @since.', array('@since' => date('r', $since)));
  }
  else {
    $markup .= t('Hookio is not running.');
  }
  $markup .= '</h3>';

  $form['hookio_running'] = array(
    '#markup' => $markup,
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['start'] = array(
    '#type' => 'submit',
    '#value' => t('Save and (re)start Hook.io'),
  );
  $form['actions']['stop'] = array(
    '#type' => 'submit',
    '#value' => t('Save and stop Hook.io'),
  );
  return $form;
}

function hookio_machinename_exists() {
  return FALSE;
}

function hookio_settings_submit($form, &$form_state) {
  $values = $form_state['values'];
  variable_set('hookio_id', $values['hookio_id']);
  variable_set('hookio_machinename', $values['hookio_machinename']);
  variable_set('hookio_host', $values['hookio_host']);
  variable_set('hookio_port', $values['hookio_port']);
  variable_set('hookio_mdns', $values['hookio_mdns']);
  variable_set('hookio_authorize', $values['hookio_authorize']);
  variable_set('hookio_user', $values['hookio_user']);
  if (strlen($values['hookio_password'])) {
    variable_set('hookio_password', $values['hookio_password']);
  }
  variable_set('hookio_server_role', $values['hookio_server_role']);
  variable_set('hookio_master_config', $values['hookio_master_config']);
  variable_set('hookio_watch_file_dir', $values['hookio_watch_file_dir']);
  variable_set('hookio_drush_path', $values['hookio_drush_path']);
  drupal_set_message(t('Settings has been updated.'));
  if ($values['op'] == t('Save and (re)start Hook.io')) {
    $config = hookio_read_master_file();
    $machine_name = hookio_get_machine_name();
    $config[$machine_name] = array(
      'site-url' => url('', array('absolute' => TRUE)),
      'drupal-base-path' => DRUPAL_ROOT,
      'drush-path' => variable_get('hookio_drush_path', '/usr/bin/drush'),
      'start-time' => REQUEST_TIME // Make sure the config changes so that it restarts.
    );
    touch (hookio_get_watch_file()); // make sure watch file is there.
    hookio_write_master_file($config);

  }
  elseif ($values['op'] == t('Save and stop Hook.io')) {
    $config = hookio_read_master_file();
    $machine_name = hookio_get_machine_name();
    unset ($config[$machine_name]);
    hookio_write_master_file($config);
    unlink (hookio_get_watch_file());
  }
}




