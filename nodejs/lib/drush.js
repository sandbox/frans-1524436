//var sys = require('util')
var sysexec = require('child_process').exec;

var siteUrl = '';
var basePath = '';
var drushPath = '/usr/bin/drush';

var settings = function(options) {
  siteUrl  = options.siteUrl || '';
  basePath = options.basePath || '';
  drushPath = options.drushPath || drushPath;
}

var exec = function (command, callback) {
  if (typeof callback == 'undefined') {
    callback = dump;
  }
  sysexec(drushPath + " -r " + basePath + " -l " + siteUrl + " " + command, callback);
};



var dump = function (error, stdout, stderr) {
  //console.log(stdout);
};


exports.exec = exec;
exports.settings = settings;


