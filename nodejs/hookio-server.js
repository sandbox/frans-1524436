
var server_conf = require('nconf');
server_conf.env().argv().use('memory');
var fs = require('fs');
var bootstrap = require('async');
var drush = require('./lib/drush');
var hookio = require('hook.io');
var hook;

var listenerCallback = function(listener) {
  var listener = listener;
  this.apply = function (data) {
    var l = new Buffer(listener).toString('base64');
    var e = new Buffer(this.event).toString('base64');
    var d = new Buffer(JSON.stringify(data)).toString('base64');
    drush.exec("hookio-receive '" + l + "' '" + e + "' '" + d + "'", function (error, stdout, stderr) {
    });
  }
}

var emitter = function() {
  drush.exec('hookio-get-next-emit', function (error, stdout, stderr) {
    if (!error) {
      if (stdout.length) {
        try {
          var ev = JSON.parse(new Buffer(stdout, 'base64').toString('utf8'));
          hook.emit(ev.event, ev.data);
          drush.exec("hookio-confirm-emit '" + ev.heid + "'", function (error, stdout, stderr) {
          });
          emitter(); // keep running till all events are gone.
        }
        catch(err) {
          console.log(err);
        }
      }
    }
  });
}

// Bootstrap serie
bootstrap.series([

    // prepare Drush calls
    function(done){
      var configdata = fs.readFileSync(server_conf.get('config-file'));
      try {
        var instances = JSON.parse(configdata);
        var instance = instances[server_conf.get('name')];
        for (k in instance) {
          server_conf.set(k, instance[k]);
        }
      }
      catch (err) {
        console.log('There has been an error parsing your JSON in' + server_conf.get('config-file') + '.');
        console.log(err);
      }
      drush.settings({
        siteUrl: server_conf.get('site-url'),
        basePath: server_conf.get('drupal-base-path'),
        drushPath: server_conf.get('drush-path'),
      });
      done();
    },

    // Fetch settings from Drupal by Drush
    function(done) {
      drush.exec('vget hookio', function (error, stdout, stderr) {
        if (!error) {
          lines = stdout.split("\n");
          for (line in lines) {
            var data = lines[line].split(': ');
            if (data.length !== 2) {
              continue;
            }
            server_conf.set(data[0], JSON.parse(data[1]));
          }
        }
        done();
      });
    },

    // Create Server
    function(done) {
      var opt = {
        'name': server_conf.get('hookio_machinename'),
        'silent': true,
        'nocheck': true,
        'debug': false,
        'autoheal': true,
        'ignoreSTDIN': true
      }
      if (server_conf.get('hookio_port').length) {
        opt['hook-port'] = parseInt(server_conf.get('hookio_port'));
      }
      if (server_conf.get('hookio_host').length) {
        opt['hook-host'] = server_conf.get('hookio_host');
      }
      if (server_conf.get('hookio_mdns')) {
        opt['m'] = true;
      }
      if (server_conf.get('hookio_authorize')) {
        opt['authorize'] = true;
        if (server_conf.get('hookio_user').length) {
          opt['user'] = server_conf.get('hookio_user');
        }
        if (server_conf.get('hookio_password').length) {
          opt['password'] = server_conf.get('hookio_password');
        }
      }
      hook = hookio.createHook(opt);
      hook.on('*::ping', function(data, callback, sender) {
        hook.emit('pong', data);
      })
      done();
    },

    // Create Listners
    function(done) {
      drush.exec('hookio-listners-list', function (error, stdout, stderr) {
        if (!error) {
          lines = stdout.split("\n");
          for (line in lines) {
            var e = lines[line];
            if (e.length) {
              var lcb = new listenerCallback(e);
              hook.on(e, lcb.apply);
            }
          }
        }
        done();
      });
    },

    // Create FileWatch
    function(done) {
      var fileName = server_conf.get('hookio_watch_file_dir') + '/' + server_conf.get('hookio_machinename') + '.watch';
      fs.writeFile(fileName, server_conf.get('hookio_machinename'), function(err) {
        if(err) {
          console.log(err);
        }
      });
      fs.chmodSync(fileName, '777');
      fs.watchFile(fileName, function(curr, prev) {
        if ((+curr.mtime - +prev.mtime) > 500) { // at least 0.5 seconds between emits (emitter is a loop).
          emitter();
        }
      });
      done();
    },
  ],
  function(err) {
    if(err) {
      throw new Error(err);
    }
    // start the server
    switch(server_conf.get('hookio_server_role')) {
      case 'listen':
        hook.listen();
        break;
      case 'connect':
        hook.connect();
        break;
      default:
        hook.start();
        break;
    }
  }
);
