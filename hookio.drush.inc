<?php

function hookio_drush_command() {
  $items = array();

  $items['hookio-listners-list'] = array(
    'description' => "List all events drupal wants to receive.",
    'aliases' => array('hll'),
  );
  $items['hookio-get-next-emit'] = array(
    'description' => "Returns next emit or empty when there is none.",
    'aliases' => array('hgne'),
  );
  $items['hookio-confirm-emit'] = array(
    'description' => "List all events drupal wants to send.",
    'aliases' => array('hce'),
    'arguments' => array(
      'heid' => 'The Hookio Event ID',
    ),
  );
  $items['hookio-receive'] = array(
    'description' => "List all events drupal wants to send.",
    'aliases' => array('hr'),
    'arguments' => array(
      'event' => 'The incomming event base64 JSON',
      'data' => 'The incomming data base64 JSON',
    ),
  );

  return $items;
}

function drush_hookio_listners_list() {
  foreach (hookio_listeners_info() as $event => $info) {
    print $event ."\n";
  }
}

function drush_hookio_receive($listener, $event, $data = NULL) {
  $listener = base64_decode($listener);
  $event = base64_decode($event);
  $data = drupal_json_decode(base64_decode($data));
  hookio_receive($listener, $event, $data);
}

function drush_hookio_get_next_emit() {
  $query = db_select('hookio_events', 'he')
    ->fields('he')
    ->condition('role', HOOKIO_ROLE_SENDER)
    ->condition('status', HOOKIO_STATUS_CREATED)
    ->range(0, 1)
    ->orderBy('time')
    ->execute();
  if ($event = $query->fetchAssoc()) {
    $save = array(
      'heid' => $event['heid'],
      'status' => HOOKIO_STATUS_FIRED,
    );
    drupal_write_record('hookio_events', $save, 'heid');
    $return = array(
      'heid' => $event['heid'],
      'event' => $event['event'],
      'data' => drupal_json_decode($event['data'])
    );
    print base64_encode(drupal_json_encode($return));
  }
}

function drush_hookio_confirm_emit($heid) {
  $save = array(
    'heid' => $heid,
    'status' => HOOKIO_STATUS_CONFIRMED,
  );
  drupal_write_record('hookio_events', $save, 'heid');
}