<?php


/**
 * hookio event call backs
 *
 * return TRUE == success
 * return NULL == success
 * return FALSE == no success, retry
 */
function hookio_test_example($data, $event, $listener) {
  $return = array(TRUE, FALSE, NULL);
  $key = array_rand($return, 1);
  watchdog('hookio_example', 'Hookio test returned: @return', array('@return' => strtoupper(var_export($return[$key], TRUE))));
  return $return[$key];
}

/**
 * Fired by drush as soon as the event arrives.
 */
function hookio_test_example2($data, $event, $listener) {
  $return = array(TRUE, FALSE, NULL);
  $key = array_rand($return, 1);
  watchdog('hookio_example', 'Hookio example2 returned: @return', array('@return' => strtoupper(var_export($return[$key], TRUE))));
  return $return[$key];
}